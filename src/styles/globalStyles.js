import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
  :root {
    --primary-color: #0070f3;
    --secondary-color: #005bb5;
    --error-color: red;
    --card-color: #000000;
    --grey-color: #929292;
  }

  body {
    font-family: 'Nunito', 'Roboto';
  }

  h1, h2, h3, h4, h5, h6 {
    font-family: 'Nunito', 'Roboto';
  }
`;

export default GlobalStyle;