import axios from 'axios';
import Cookies from 'js-cookie';

const API_URL = 'http://localhost:8080/users';

export const validateToken = async () => {
  const token = Cookies.get('authToken');
  if (!token) {
    return null;
  }

  try {
    const response = await axios.get(`${API_URL}/validateToken`, {
      headers: {
        'Authorization': `Bearer ${token}`
      },
      withCredentials: true
    });
    return response.status === 200 ? { isAuthenticated: true } : null;
  } catch (error) {
    console.error('Token validation failed:', error);
    return null;
  }
};

export const logout = async () => {
  const token = Cookies.get('authToken');
  if (!token) {
    return;
  }

  try {
    await axios.post(`${API_URL}/logout`, {}, {
      headers: {
        'Authorization': `Bearer ${token}`
      },
      withCredentials: true
    });
    Cookies.remove('authToken');
    window.location.href = '/login';
  } catch (error) {
    console.error('Logout failed:', error);
  }
};


export const getAllUsers = async () => {
    const response = await fetch(`${apiUrl}/users`);
    if (response.ok) {
        return await response.json();
    } else {
        throw new Error('Failed to fetch users');
    }
};

