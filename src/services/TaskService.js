import axios from 'axios';

const API_BASE_URL = 'http://localhost:8081/tasks';

export const getTasksByUserId = (userId) => axios.get(`${API_BASE_URL}/user/${userId}`);
export const createTask = (task) => axios.post(API_BASE_URL, task);
export const updateTask = (taskId, task) => axios.put(`${API_BASE_URL}/${taskId}`, task);
export const deleteTask = (taskId) => axios.delete(`${API_BASE_URL}/${taskId}`);
export const updateTaskImage = (taskId, imageData) => {
  const formData = new FormData();
  formData.append('file', imageData);

  return axios.put(`${API_BASE_URL}/${taskId}/image`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  });
};
export const deleteTaskImage = (taskId) => axios.delete(`${API_BASE_URL}/${taskId}/image`);

