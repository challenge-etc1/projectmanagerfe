import { useEffect } from 'react';
import { useAuth } from './AuthProvider';
import Swal from 'sweetalert2';

const ProtectedRoute = ({ children }) => {
  const { user, isLoading } = useAuth();

  useEffect(() => {
    if (!isLoading && !user) {
      console.log('No user found, redirecting to login');
      window.location.href = "http://localhost:3000/login";
    }
  }, [user, isLoading]);

  if (isLoading) {
    Swal.fire({
      title: 'Loading...',
      text: 'Please wait while we verify your credentials.',
      didOpen: () => {
        Swal.showLoading();
      },
      allowOutsideClick: false,
      allowEscapeKey: false,
      allowEnterKey: false
    });
    return null;
  }

  return user ? children : null;
};

export default ProtectedRoute;