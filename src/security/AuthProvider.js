import React, { createContext, useContext, useState, useEffect } from 'react';
import axios from 'axios';
import Cookies from 'js-cookie';

const AuthContext = createContext(null);

export const useAuth = () => useContext(AuthContext);

export const AuthProvider = ({ children }) => {
  const [user, setUser] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  const validateToken = async () => {
    const token = Cookies.get('authToken');
    if (!token) {
      setUser(null);
      setIsLoading(false);
      return;
    }

    try {
      const response = await axios.get('http://localhost:8080/users/validateToken', {
        headers: {
          'Authorization': `Bearer ${token}`
        },
        withCredentials: true
      });
      if (response.status === 200) {
        setUser({ isAuthenticated: true });
      } else {
        setUser(null);
      }
    } catch (error) {
      console.error('Token validation failed:', error);
      setUser(null);
    }
    setIsLoading(false);
  };

  useEffect(() => {
    validateToken();
  }, []);

  const login = async () => {
    await validateToken();
  };

  const logout = async () => {
    try {
      const token = Cookies.get('authToken');
      await axios.post('http://localhost:8080/users/logout', {}, {
        headers: {
          'Authorization': `Bearer ${token}`
        },
        withCredentials: true
      });
    } catch (error) {
      console.error('Logout failed:', error);
    }
    Cookies.remove('authToken');
    setUser(null);
    window.location.href = '/login';
  };

  return (
    <AuthContext.Provider value={{ user, login, logout, isLoading }}>
      {children}
    </AuthContext.Provider>
  );
};