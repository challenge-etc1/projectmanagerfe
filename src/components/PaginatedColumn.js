import React, { useState } from "react";
import Column from "./Column";
import styled from "styled-components";

const PaginationButton = styled.button`
  padding: 8px 16px;
  background-color: #0070f3;
  color: #ffffff;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  margin-left: 8px;

  &:hover {
    background-color: #005bb5;
  }
`;

const PaginatedColumnContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  max-width: 33%;
  gap: 6px;
`;

const PaginationContainer = styled.div`
  display: flex;
  justify-content: space-between;
  min-height: 40px;
`;

const PaginatedColumn = ({ title, tasks, handleDragOver, handleDrop, handleDragStart, handleSelectTask }) => {
  const [page, setPage] = useState(0);
  const tasksPerPage = 10;

  const handleNextPage = () => setPage(page + 1);
  const handlePreviousPage = () => setPage(page - 1);

  const paginatedTasks = tasks.slice(page * tasksPerPage, (page + 1) * tasksPerPage);

  return (
    <PaginatedColumnContainer>
      <Column
        title={title}
        tasks={paginatedTasks}
        handleDragOver={handleDragOver}
        handleDrop={handleDrop}
        handleDragStart={handleDragStart}
        handleSelectTask={handleSelectTask}
      />
      <PaginationContainer>
        {page > 0 && <PaginationButton onClick={handlePreviousPage}>Previous</PaginationButton>}
        {(page + 1) * tasksPerPage < tasks.length && <PaginationButton onClick={handleNextPage}>Next</PaginationButton>}
      </PaginationContainer>
    </PaginatedColumnContainer>
  );
};

export default PaginatedColumn;