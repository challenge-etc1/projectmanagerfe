import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { FaEdit, FaSave } from "react-icons/fa";
import EditPasswordModal from "./EditPasswordModal";
import * as userService from '../services/UserService';

const UserInfoContainer = styled.div`
  padding: 48px;
  background-color: #ffffff;
  border-radius: 16px;
  box-shadow: 0 4px 12px rgba(0, 0, 0, 0.1);
  max-width: 800px;
  margin: 40px auto;
`;

const UserInfoRow = styled.div`
  display: flex;
  justify-content: space-between;
  gap: 10px;
  align-items: center;
  margin: 10px;
  position: relative;
`;

const Label = styled.label`
  font-weight: bold;
  font-size: 18px;
  flex: 1;
`;

const Value = styled.span`
  font-size: 18px;
  flex: 2;
`;

const Input = styled.input`
  padding: 12px;
  font-size: 18px;
  border-radius: 8px;
  border: 1px solid #ced4da;
  flex: 2;
`;

const Select = styled.select`
  padding: 12px;
  font-size: 18px;
  border-radius: 8px;
  border: 1px solid #ced4da;
  flex: 2;
`;

const EditIcon = styled(FaEdit)`
  cursor: pointer;
  margin-left: 8px;
  color: #0070f3;

  &:hover {
    color: #005bb5;
  }
`;

const SaveIcon = styled(FaSave)`
  cursor: pointer;
  margin-left: 8px;
  color: #0070f3;

  &:hover {
    color: #005bb5;
  }
`;

const Button = styled.button`
  padding: 12px 24px;
  background-color: #0070f3;
  color: white;
  border: none;
  border-radius: 8px;
  cursor: pointer;
  font-size: 18px;
  margin-top: 16px;

  &:hover {
    background-color: #005bb5;
  }
`;

const UserInfo = ({ userId }) => {
  const [userInfo, setUserInfo] = useState({
    username: "", phone: "", age: null, gender: "", password: "********"
  });
  const [isPasswordModalOpen, setIsPasswordModalOpen] = useState(false);
  const [isEditing, setIsEditing] = useState({ phone: false, age: false, gender: false });

  useEffect(() => {
    userService.getUserById(userId)
      .then(data => setUserInfo({ ...data, password: "********" }))
      .catch(error => console.error('Error fetching user data:', error));
  }, [userId]);

  const handleInputChange = (e, field) => {
    setUserInfo({ ...userInfo, [field]: e.target.value });
  };

  const handleEditToggle = (field) => {
    setIsEditing(prev => ({ ...prev, [field]: !prev[field] }));
  };

  return (
    <UserInfoContainer>
      <UserInfoRow>
        <Label>Username:</Label>
        <Value>{userInfo.username}</Value>
      </UserInfoRow>
      <UserInfoRow>
        <Label>Phone:</Label>
        {isEditing.phone ? (
          <Input
            type="text"
            value={userInfo.phone}
            onChange={(e) => handleInputChange(e, "phone")}
            onBlur={() => handleEditToggle("phone")}
          />
        ) : (
          <>
            <Value>{userInfo.phone}</Value>
            <EditIcon onClick={() => handleEditToggle("phone")} />
          </>
        )}
      </UserInfoRow>
      <UserInfoRow>
        <Label>Age:</Label>
        {isEditing.age ? (
          <Input
            type="number"
            value={userInfo.age}
            onChange={(e) => handleInputChange(e, "age")}
            onBlur={() => handleEditToggle("age")}
          />
        ) : (
          <>
            <Value>{userInfo.age}</Value>
            <EditIcon onClick={() => handleEditToggle("age")} />
          </>
        )}
      </UserInfoRow>
      <UserInfoRow>
        <Label>Gender:</Label>
        {isEditing.gender ? (
          <Select
            value={userInfo.gender}
            onChange={(e) => handleInputChange(e, "gender")}
            onBlur={() =>handleEditToggle("gender")} />
          ) : (
            <>
              <Value>{userInfo.gender}</Value>
              <EditIcon onClick={() => handleEditToggle("gender")} />
            </>
          )}
        </UserInfoRow>
        <UserInfoRow>
          <Label>Password:</Label>
          <Button onClick={() => setIsPasswordModalOpen(true)}>Change Password</Button>
        </UserInfoRow>
        <EditPasswordModal
          isOpen={isPasswordModalOpen}
          onClose={() => setIsPasswordModalOpen(false)}
          onSave={(newPassword) => {
            console.log('Password changed to:', newPassword); // Implement actual password update logic
            setIsPasswordModalOpen(false);
          }}
        />
      </UserInfoContainer>
    );
  };

  export default UserInfo;