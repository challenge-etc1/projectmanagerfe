import React from "react";
import styled from "styled-components";

const FormContainer = styled.div`
  background-color: #e9ecef;
  padding: 16px 24px;
  display: flex;
  align-items: center;
  gap: 16px;
`;

const Input = styled.input`
  flex: 1;
  padding: 8px;
  border-radius: 4px;
  border: 1px solid #ced4da;
`;

const Button = styled.button`
  padding: 8px 16px;
  background-color: #0070f3;
  color: #ffffff;
  border: none;
  border-radius: 4px;
  cursor: pointer;

  &:hover {
    background-color: #005bb5;
  }

  &:disabled {
    background-color: #cccccc;
    cursor: not-allowed;
  }
`;

const NewTaskForm = ({
  newTaskTitle,
  setNewTaskTitle,
  handleCreateTask
}) => (
  <FormContainer>
    <Input
      placeholder="Enter new task title"
      value={newTaskTitle}
      onChange={(e) => setNewTaskTitle(e.target.value)}
      onKeyPress={(e) => e.key === 'Enter' && newTaskTitle && handleCreateTask()}
    />
    <Button 
      onClick={() => {
        if (newTaskTitle) {
          handleCreateTask();
          setNewTaskTitle("");
        }
      }}
      disabled={!newTaskTitle.trim()}
    >
      Create Task
    </Button>
  </FormContainer>
);

export default NewTaskForm;