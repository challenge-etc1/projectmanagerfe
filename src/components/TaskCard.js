import React from "react";
import styled from "styled-components";

const Card = styled.div`
  padding: 16px;
  background-color: #f8f9fa;
  border: 1px solid #dee2e6;
  border-radius: 8px;
  cursor: pointer;
  margin-bottom: 8px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  color: var(--card-color);
`;

const CardContent = styled.div`
  font-size: 16px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  flex: 1;
`;

const TaskCard = ({ task, handleDragStart, handleSelectTask }) => (
  <Card draggable onDragStart={(e) => handleDragStart(e, task)} onClick={() => handleSelectTask(task)}>
    <CardContent>{task.title}</CardContent>
  </Card>
);

export default TaskCard;
