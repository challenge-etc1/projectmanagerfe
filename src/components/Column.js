import React from "react";
import styled from "styled-components";
import TaskCard from "./TaskCard";

const ColumnContainer = styled.div`
  background-color: #ffffff;
  border-radius: 8px;
  box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1);
  padding: 16px;
  display: flex;
  flex-direction: column;
  flex: 1;
  margin: 0 8px;
`;

const ColumnTitle = styled.h2`
  font-size: 20px;
  font-weight: bold;
  margin-bottom: 16px;

  @media (max-width: 768px) {
  font-size: 13px;
  }
`;

const TasksContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

const Column = ({ title, tasks, handleDragOver, handleDrop, handleDragStart, handleSelectTask }) => (
  <ColumnContainer onDragOver={(e) => handleDragOver(e, title.toLowerCase().replace(" ", "-"))} onDrop={(e) => handleDrop(e, title.toLowerCase().replace(" ", "-"))}>
    <ColumnTitle>{title}</ColumnTitle>
    <TasksContainer>
      {tasks.map(task => (
        <TaskCard key={task.id} task={task} handleDragStart={handleDragStart} handleSelectTask={handleSelectTask} />
      ))}
    </TasksContainer>
  </ColumnContainer>
);

export default Column;
