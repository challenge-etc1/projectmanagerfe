import React, { useState, useCallback } from "react";
import Cropper from "react-easy-crop";
import { getCroppedImg } from "../utils/CropImage";
import { useDropzone } from "react-dropzone";
import styled from "styled-components";

const UploaderContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 16px;
  width: 100%;
`;

const Dropzone = styled.div`
  border: 2px dashed #ccc;
  color: var(--card-color);
  padding: 20px;
  width: 100%;
  text-align: center;
`;

const CropContainer = styled.div`
  width: 100%;
  max-width: 400px; /* Ensure it does not exceed the modal width */
  height: 300px; /* Set a specific height */
  background: #333;
  position: relative; /* Ensure positioning for the cropper */
`;

const ButtonContainer = styled.div`
  display: flex;
  justify-content: space-around;
  margin-top: 10px;
  width: 100%;
`;

const Button = styled.button`
  width: 30%; /* Set width to 30% */
  padding: 8px 16px;
  background-color: #0070f3;
  color: white;
  border: none;
  border-radius: 4px;
  cursor: pointer;

  &:hover {
    background-color: #005bb5;
  }
`;

const CancelButton = styled.button`
  width: 30%; /* Set width to 30% */
  padding: 8px 16px;
  background-color: #ccc;
  color: var(--card-color);
  border: none;
  border-radius: 4px;
  cursor: pointer;

  &:hover {
    background-color: #999;
  }
`;

const ImageUploader = ({ onImageUpload }) => {
  const [imageSrc, setImageSrc] = useState(null);
  const [croppedArea, setCroppedArea] = useState(null);
  const [crop, setCrop] = useState({ x: 0, y: 0 });
  const [zoom, setZoom] = useState(1);

  const onDrop = useCallback((acceptedFiles) => {
    const file = acceptedFiles[0];
    const reader = new FileReader();
    reader.addEventListener("load", () => setImageSrc(reader.result));
    reader.readAsDataURL(file);
  }, []);

  const { getRootProps, getInputProps } = useDropzone({
    onDrop,
    accept: "image/*",
  });

  const onCropComplete = useCallback((croppedArea, croppedAreaPixels) => {
    setCroppedArea(croppedAreaPixels);
  }, []);

  const handleUpload = async () => {
    const croppedImage = await getCroppedImg(imageSrc, croppedArea);
    onImageUpload(croppedImage);
    setImageSrc(null);
  };

  const handleCancel = () => {
    setImageSrc(null);
    setCroppedArea(null);
    setCrop({ x: 0, y: 0 });
    setZoom(1);
  };

  return (
    <UploaderContainer>
      {!imageSrc ? (
        <Dropzone {...getRootProps()}>
          <input {...getInputProps()} />
          <p>Drag 'n' drop an image here, or click to select an image</p>
        </Dropzone>
      ) : (
        <div style={{ width: '100%' }}>
          <CropContainer>
            <Cropper
              image={imageSrc}
              crop={crop}
              zoom={zoom}
              aspect={1}
              onCropChange={setCrop}
              onZoomChange={setZoom}
              onCropComplete={onCropComplete}
            />
          </CropContainer>
          <ButtonContainer>
            <CancelButton onClick={handleCancel}>Cancel</CancelButton>
            <Button onClick={handleUpload}>Upload Image</Button>
          </ButtonContainer>
        </div>
      )}
    </UploaderContainer>
  );
};

export default ImageUploader;
