import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { FaUser } from "react-icons/fa";

const HeaderContainer = styled.header`
  background-color: #0070f3;
  color: #ffffff;
  padding: 16px 24px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const Title = styled(Link)`
  font-size: 24px;
  font-weight: bold;
  color: #ffffff;
  text-decoration: none;

  &:hover {
    color: #e0e0e0;
  }
`;

const ProfileButton = styled(Link)`
  display: flex;
  align-items: center;
  background-color: #005bb5;
  color: #ffffff;
  padding: 8px 16px;
  border-radius: 4px;
  text-decoration: none;
  position: relative;

  &:hover {
    background-color: #003f7f;
  }

  &:hover::after {
    content: "Profile";
    position: absolute;
    bottom: -24px;
    left: 50%;
    transform: translateX(-50%);
    background-color: #333;
    color: #fff;
    padding: 4px 8px;
    border-radius: 4px;
    white-space: nowrap;
    font-size: 14px;
  }
`;

const Icon = styled(FaUser)`
`;

const Header = () => (
  <HeaderContainer>
    <Title to="/">Project Manager</Title>
    <ProfileButton to="/profile">
      <Icon />
    </ProfileButton>
  </HeaderContainer>
);

export default Header;
