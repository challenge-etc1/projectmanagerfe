import React, { useState, useEffect } from "react";
import styled from "styled-components";
import Swal from 'sweetalert2';
import { FaEdit, FaSave } from 'react-icons/fa';
import ImageUploader from "./ImageUploader";
import * as taskService from '../services/TaskService';
import * as userService from '../services/UserService';

const ModalOverlay = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0, 0, 0, 0.5);
  display: flex;
  justify-content: center;
  align-items: center;
  overflow-y: auto;
`;

const ModalContent = styled.div`
  background-color: #ffffff;
  border-radius: 8px;
  box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1);
  padding: 24px;
  width: 400px;
  max-width: 100%;
  box-sizing: border-box;
`;

const ModalHeader = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 16px;
`;

const ModalTitleContainer = styled.div`
  display: flex;
  align-items: center;
  flex: 1;
  min-width: 0;
`;

const ModalTitle = styled.h2`
  font-size: 20px;
  font-weight: bold;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  flex: 1;
  margin-right: 8px;
  color: var(--card-color);
`;

const ModalBody = styled.div`
  display: grid;
  gap: 16px;
`;

const ModalFooter = styled.div`
  display: flex;
  justify-content: space-between;
  gap: 8px;
  padding: 16px 0px 0px;
`;

const Input = styled.input`
  padding: 8px;
  border-radius: 4px;
  border: 1px solid #ced4da;
  width: 100%;
`;

const Textarea = styled.textarea`
  padding: 8px;
  border-radius: 4px;
  border: 1px solid #ced4da;
  width: 100%;
  box-sizing: border-box;
  resize: vertical;
`;

const Select = styled.select`
  padding: 8px;
  border-radius: 4px;
  border: 1px solid #ced4da;
  width: 100%;
  box-sizing: border-box;
`;

const Button = styled.button`
  padding: 8px 16px;
  background-color: #0070f3;
  color: #ffffff;
  border: none;
  border-radius: 4px;
  cursor: pointer;

  &:hover {
    background-color: #005bb5;
  }
`;

const DescriptionContainer = styled.div`
  width: 100%;
  transition: width 0.3s ease;
  box-sizing: border-box;
`;

const TaskModal = ({ selectedTask, handleCloseModal, handleInputChange, handleUpdateTask, handleDragTask, handleDeleteTask }) => {
  const [isEditingTitle, setIsEditingTitle] = useState(false);
  const [taskImage, setTaskImage] = useState(selectedTask.image || null);
  const [users, setUsers] = useState([]);

  useEffect(() => {
    fetchUsers();
  }, []);

  const fetchUsers = async () => {
    try {
      const response = await userService.getAllUsers();
      setUsers(response.data);
    } catch (error) {
      console.error('Error fetching users', error);
    }
  };


  const handleImageUpload = async (croppedImage) => {
    setTaskImage(croppedImage);
    try {
      await taskService.updateTaskImage(selectedTask.id, croppedImage);
      handleInputChange({ target: { value: croppedImage } }, "image");
    } catch (error) {
      console.error('Error uploading image', error);
      Swal.fire('Error', 'There was an error uploading the image.', 'error');
    }
  };

  const handleDelete = () => {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#3085d6',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        handleDeleteTask(selectedTask.id);
        Swal.fire('Deleted!', 'Your task has been deleted.', 'success');
        handleCloseModal();
      }
    });
  };

  return (
    <ModalOverlay>
      <ModalContent>
        <ModalHeader>
          <ModalTitleContainer>
            {isEditingTitle ? (
              <Input
                type="text"
                value={selectedTask.title}
                onChange={(e) => handleInputChange(e, "title")}
                placeholder="Task Title"
              />
            ) : (
              <ModalTitle title={selectedTask.title}>{selectedTask.title}</ModalTitle>
            )}
            {isEditingTitle ? (
              <FaSave onClick={() => setIsEditingTitle(false)} style={{ cursor: 'pointer' }} />
            ) : (
              <FaEdit onClick={() => setIsEditingTitle(true)} style={{ cursor: 'pointer' }} />
            )}
          </ModalTitleContainer>
        </ModalHeader>
        <ModalBody>
          <div>
            <label>Status:</label>
            <Select value={selectedTask.status} onChange={(e) => {
              handleInputChange(e, "status");
              handleDragTask(selectedTask.id, e.target.value);
            }}>
              <option value="pending">Pending</option>
              <option value="in-progress">In Progress</option>
              <option value="completed">Completed</option>
            </Select>
          </div>
          <DescriptionContainer>
            <label>Description:</label>
            <Textarea value={selectedTask.description} onChange={(e) => handleInputChange(e, "description")} rows={4} />
          </DescriptionContainer>
          <div>
            <label>Due Date:</label>
            <Input type="date" value={selectedTask.dueDate} onChange={(e) => handleInputChange(e, "dueDate")} />
          </div>
          <div>
          <label>Assigned To:</label>
          <Select value={selectedTask.assignedTo} onChange={(e) => handleInputChange(e, "assignedTo")}>
            {users.map(user => (
              <option key={user.id} value={user.username}>{user.username}</option> 
            ))}
          </Select>
        </div>
          <div>
            <label>Task Image:</label>
            <ImageUploader onImageUpload={handleImageUpload} />
            {taskImage && <img src={taskImage} alt="Task" style={{ marginTop: "10px", maxWidth: "100%" }} />}
          </div>
        </ModalBody>
        <ModalFooter>
          <Button onClick={handleDelete} style={{ backgroundColor: 'var(--error-color)' }}>Delete</Button>
          <Button onClick={handleCloseModal} style={{ backgroundColor: 'var(--grey-color)'}}>Cancel</Button>
          <Button onClick={handleUpdateTask}>Save</Button>
        </ModalFooter>
      </ModalContent>
    </ModalOverlay>
  );
};

export default TaskModal;
