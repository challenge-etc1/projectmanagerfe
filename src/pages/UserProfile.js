import React from "react";
import UserInfo from "../components/UserInfo";
import styled from "styled-components";

const PageContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
  background-color: #f0f0f0;
`;

const UserProfile = () => {
  return (
    <PageContainer>
      <UserInfo />
    </PageContainer>
  );
};

export default UserProfile;
