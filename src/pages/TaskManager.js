import React, { useState, useEffect } from "react";
import { TaskModal, NewTaskForm } from '../components';
import PaginatedColumn from "../components/PaginatedColumn";
import styled from "styled-components";
import * as taskService from "../services/TaskService.js"

const Container = styled.div`
  display: flex;
  flex-direction: column;
  height: 94vh;
  background-color: #f0f0f0;
`;

const MainContent = styled.div`
  display: flex;
  flex: 1;
  padding: 24px;
`;

export default function TaskManagement() {
  const [tasks, setTasks] = useState([]);
  const [selectedTask, setSelectedTask] = useState(null);

  useEffect(() => {
    fetchTasks();
  }, []);

  const fetchTasks = async () => {
    try {
      const response = await taskService.getTasksByUserId(1);
      setTasks(response.data);
    } catch (error) {
      console.error('Error fetching tasks', error);
    }
  };

  const handleCreateTask = async (taskData) => {
    try {
      const response = await taskService.createTask(taskData);
      setTasks([...tasks, response.data]);
    } catch (error) {
      console.error('Error creating task', error);
    }
  };

  const handleUpdateTask = async (task) => {
    try {
      const response = await taskService.updateTask(task.id, task);
      setTasks(tasks.map(t => t.id === task.id ? response.data : t));
      setSelectedTask(null);
    } catch (error) {
      console.error('Error updating task', error);
    }
  };

  const handleDeleteTask = async (taskId) => {
    try {
      await taskService.deleteTask(taskId);
      setTasks(tasks.filter(task => task.id !== taskId));
    } catch (error) {
      console.error('Error deleting task', error);
    }
  };

  const handleDragTask = async (id, status) => {
    const task = tasks.find(t => t.id === id);
    if (task) {
      const updatedTask = { ...task, status };
      await handleUpdateTask(updatedTask);
    }
  };

  const handleDragOver = (e) => {
    e.preventDefault();
    e.dataTransfer.dropEffect = "move";
  };

  const handleDrop = (e, status) => {
    e.preventDefault();
    const taskId = parseInt(e.dataTransfer.getData("text/plain"));
    handleDragTask(taskId, status);
  };

  const handleDragStart = (e, task) => {
    e.dataTransfer.setData("text/plain", task.id);
  };

  return (
    <Container>
      <MainContent>
        <PaginatedColumn
          title="Pending"
          tasks={tasks.filter((task) => task.status === "pending")}
          handleDragOver={handleDragOver}
          handleDrop={(e) => handleDrop(e, "pending")}
          handleDragStart={handleDragStart}
          handleSelectTask={setSelectedTask}
        />
        <PaginatedColumn
          title="In Progress"
          tasks={tasks.filter((task) => task.status === "in-progress")}
          handleDragOver={handleDragOver}
          handleDrop={(e) => handleDrop(e, "in-progress")}
          handleDragStart={handleDragStart}
          handleSelectTask={setSelectedTask}
        />
        <PaginatedColumn
          title="Completed"
          tasks={tasks.filter((task) => task.status === "completed")}
          handleDragOver={handleDragOver}
          handleDrop={(e) => handleDrop(e, "completed")}
          handleDragStart={handleDragStart}
          handleSelectTask={setSelectedTask}
        />
      </MainContent>
      <NewTaskForm
        handleCreateTask={handleCreateTask}
      />
      {selectedTask && (
        <TaskModal
          selectedTask={selectedTask}
          handleCloseModal={() => setSelectedTask(null)}
          handleUpdateTask={handleUpdateTask}
          handleDeleteTask={handleDeleteTask}
        />
      )}
    </Container>
  );
}
