import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import TaskManagement from "./TaskManager";
import UserProfile from "./UserProfile";
import Header from "../components/Header";
import { AuthProvider } from "../security/AuthProvider";
import ProtectedRoute from "../security/ProtectedRoute";

const HomePage = () => (
  <Router>
    <AuthProvider>
      <Header />
      <Routes>
        <Route path="/" element={
          <ProtectedRoute>
            <TaskManagement />
          </ProtectedRoute>
        } />
        <Route path="/profile" element={
          <ProtectedRoute>
            <UserProfile />
          </ProtectedRoute>
        } />
      </Routes>
    </AuthProvider>
  </Router>
);

export default HomePage;
